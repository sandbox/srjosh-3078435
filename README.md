# Migrate File Singularity

## Introduction
Once upon a time, a problem occured. The files were duplicated and munged up and editors despaired of every reusing them, and so they uploaded the same images multiple times. This module slims down the file set in both the database and file system. 

## How it works
During the file migration, this module hashes the contents of each file to get a unique file checksum. This hash is compared with database records. If a matching hash is found, then the entity reference for that file is substituted, and the IDs recorded.  If not, then the hash is recorded along with the new file ID.

Two Database tables are installed: `migrate_file_singularity_hash`, and `migrate_file_singularity_map`.

One Event Subscriber is included: _Migrate File Singularity Subscriber_.

Three process plugins are provided: _Save File Singularity_, _Match File Singularity_, and _Save Media Singularity_.

### Database Tables
### migrate_file_singularity_hash
This table records a D8 file ID, and that file's hash (see below). This allows for files to get compared based on their contents.
### migrate_file_singularity_map
If a file record is found that has a file hash recorded in `migrate_file_singularity_hash`, then the old (redundant) D7 file ID is recorded, along with the new D8 ID of that file's single record. This allows for migration lookup on duplicate files.

### Migrate File Singularity Subscriber
This file subscribes to two Migration events: `MigratePostRowSaveEvent`, and `MigrateRowDeleteEvent`  

#### MigratePostRowSaveEvent
Each time a file is migrated, save the file hash to the `migrate_file_singularity_hash` table, along with the file ID.
#### MigrateRowDeleteEvent
Each time a file is deleted in a migration rollback, remove the hash and ID data from the `migrate_file_singularity_hash` table.

### Process Plugins
#### Save File Singularity
This plugin is used on **FILE** migrations.

Using data from 'migrate_file_singularity_hash', this process plugin is what does the file hash comparison. If a match is found, meaning an identical file has already been migrated, it records the old D7 ID and the new "master" D8 file ID, then throws a `MigrateSkipRowException`. Otherwise, it returns NULL and allows auto-incrementing to create the file ID.
```yml
source:
  # This tells migration to record the file hash for duplication reduction.
  file_singularity: true
  constants:
    source_base_path: 'public://d7store'
process:
  # 9 for 'public://'.
  source_uri:
    plugin: substr
    source: uri
    start: 9

  source_path:
    plugin: concat
    delimiter: '/'
    source:
      - constants/source_base_path
      - '@source_uri'

  # We're not really looking for the fid; this is a match against the file hash.
  # If it's found, the row gets skipped; otherwise `fid` is returned as NULL.
  fid:
    plugin: save_file_singularity
    source: '@source_path'
```

#### Match File Singularity
This plugin is used on **MEDIA** migrations.

This process plugin extends the Migration Lookup plugin. When a media entity is being migrated, this plugin looks first in the `migrate_file_singularity_map` table for the source file.  If it doesn't find it there, it hands the file ID off to Migration Lookup for a standard entity reference lookup. This allows for consolidation of migrated files.
```yml
process:
  field_media_image:
    plugin: match_file_singularity
    source: field_image
    migration: example_file
    no_stub: true

```

#### Save Media Singularity
This plugin is used on **MEDIA** migrations.

This process plugin records the file's hash in a field on the _media_ entity. This is specifically geared for integration with the Migrate Media Handler module. It extends the Migration Lookup plugin, because it uses that modules functionality to lookup the related, migrated file for hashing purposes.
```yml
process:
  field_original_ref:
    plugin: save_media_singularity
    source: field_image
    migration: example_file
    no_stub: true
```
