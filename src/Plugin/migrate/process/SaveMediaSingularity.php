<?php

namespace Drupal\migrate_file_singularity\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\migrate\process\MigrationLookup;
use Drupal\migrate\Row;
use Drupal\file\Entity\File;

/**
 * Extend migration lookup to save file hashes for Media.
 *
 * Available configuration keys
 * - source: fid to convert to hash.
 * - migration: the migration in which to lookup the file
 * - no_stub: always set this.
 *
 * @MigrateProcessPlugin(
 *   id = "save_media_singularity"
 * )
 */
class SaveMediaSingularity extends MigrationLookup {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    if (is_array($value) && !empty($value['fid'])) {
      $value = $value['fid'];
    }

    // Find mapped duplicate file.
    $connection = \Drupal::database();
    $query = $connection->select('migrate_file_singularity_map', 'mfsm');
    $query->condition('mfsm.sourceid', $value, '=');
    $query->fields('mfsm', ['destid']);
    $fid = $query->execute()->fetch();

    if (empty($fid)) {
      // If no duplicate is found, run normal migration lookup.
      $fid = parent::transform($value, $migrate_executable, $row, $destination_property);
    }
    else {
      // Extract dest id from database object.
      $fid = $fid->destid;
    }
    if (!empty($fid)) {
      // Load and hash file.
      $file = File::load($fid);
      if (!empty($file)) {
        $file_url = $file->getFileUri();
        $sha1file = sha1_file($file_url);
        return $sha1file;
      }
    }

    // If no file, Provide migrate message a reason for skipping this whole row.
    $message = "No valid file or filepath: " . print_r($value, TRUE);
    throw new MigrateSkipRowException($message);
  }

}
