<?php

namespace Drupal\migrate_file_singularity\Plugin\migrate\process;

use Drupal\migrate\Plugin\migrate\process\MigrationLookup;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Extend migration lookup to redirect file references based on duplication.
 *
 * Available configuration keys
 * - migration: A single migration ID, or an array of migration IDs.
 * - source_ids: (optional) An array keyed by migration IDs with values that are
 *   a list of source properties.
 * - stub_id: (optional) Identifies the migration which will be used to create
 *   any stub entities.
 * - no_stub: (optional) Prevents the creation of a stub entity when no
 *   relationship is found in the migration map.
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 * @see \Drupal\migrate\Plugin\migrate\process\MigrationLookup
 *
 * @MigrateProcessPlugin(
 *   id = "match_file_singularity"
 * )
 */
class MatchFileSingularity extends MigrationLookup {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    if (is_array($value) && !empty($value['fid'])) {
      $source_fid = $value['fid'];
    }
    else {
      $source_fid = $value;
    }

    // Find mapped duplicate file, if it exists.
    $connection = \Drupal::database();
    $query = $connection->select('migrate_file_singularity_map', 'mfsm');
    $query->condition('mfsm.sourceid', $source_fid, '=');
    $query->fields('mfsm', ['destid']);
    $fid = $query->execute()->fetch();

    if (!empty($fid)) {
      return $fid->destid;
    }
    // If no duplicate is found, run normal migration lookup.
    return parent::transform($source_fid, $migrate_executable, $row, $destination_property);
  }

}
