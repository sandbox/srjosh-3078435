<?php

namespace Drupal\migrate_file_singularity\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * Compare files based on their hash. Skip import and save duplicate mapping.
 *
 * Available config keys:
 * - source: The source filepath.
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *   id = "save_file_singularity"
 * )
 */
class SaveFileSingularity extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (file_exists($value)) {
      $sha1file = sha1_file($value);

      // Find file reference based on hash.
      $connection = \Drupal::database();
      $query = $connection->select('migrate_file_singularity_hash', 'mfsh');
      $query->condition('mfsh.filehash', $sha1file, '=');
      $query->fields('mfsh', ['fid']);
      $fids = $query->execute();

      if (!empty($fids)) {
        foreach ($fids as $fid) {
          // Save duplicate file mapping for use with MatchFileSingularity.
          $sourceid = $row->getSource()['fid'];
          $query = $connection->insert('migrate_file_singularity_map')
            ->fields([
              'sourceid' => $sourceid,
              'destid' => $fid->fid,
            ]);
          $query->execute();
          // Provide migrate message a reason for skipping this file.
          $message = $value . " was already imported as fid: " . $fid->fid;
          throw new MigrateSkipRowException($message);
        }
      }
    }
    // If nothing, then return null and allow auto-increment to set FID.
    return NULL;
  }

}
