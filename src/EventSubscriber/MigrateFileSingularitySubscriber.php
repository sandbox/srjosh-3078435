<?php

namespace Drupal\migrate_file_singularity\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Drupal\migrate\Event\MigrateRowDeleteEvent;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Plugin\Migration;
use Drupal\Core\Database\Connection;

/**
 * Class MigrateFileSingularitySubscriber.
 *
 * Subscribes to migrate events to save hashes of files for the purpose
 * of reducing file duplication.
 *
 * @package Drupal\migrate_file_singularity\EventSubscriber
 */
class MigrateFileSingularitySubscriber implements ContainerInjectionInterface, EventSubscriberInterface {

  /**
   * Database Connection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $databaseConnection;

  /**
   * Constructs a Media Maker object.
   *
   * @param \Drupal\Core\Database\Connection $database_connection
   *   The Database Connection service.
   */
  public function __construct(Connection $database_connection) {
    $this->databaseConnection = $database_connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [];
    $events[MigrateEvents::POST_ROW_SAVE] = [
      'onPostRowSave',
    ];
    $events[MigrateEvents::POST_ROW_DELETE] = [
      'onPostRollback',
    ];
    return $events;
  }

  /**
   * Saves the file contents in a hash with the `fid`.
   *
   * @param \Drupal\migrate_file_singularity\EventSubscriber\MigratePostRowSaveEvent $event
   *   The migrate post row save event.
   */
  public function onPostRowSave(MigratePostRowSaveEvent $event) {
    $migration = $event->getMigration();
    if ($this->isFilesMigration($migration)) {
      $fid = $event->getDestinationIdValues();
      $destination = $event->getRow()->getDestination();
      $sha1file = sha1_file($destination['dest_path']);

      $connection = $this->databaseConnection;
      // Save file hash.
      $query = $connection->insert('migrate_file_singularity_hash')
        ->fields([
          'fid' => $fid[0],
          'filehash' => $sha1file,
        ]);
      $query->execute();
    }
  }

  /**
   * Clears the file hash and file map content.
   *
   * @param \Drupal\migrate\Event\MigrateRowDeleteEvent $event
   *   The migrate post row save event.
   */
  public function onPostRollback(MigrateRowDeleteEvent $event) {
    $migration = $event->getMigration();
    if ($this->isFilesMigration($migration)) {
      $id = $event->getDestinationIdValues();

      $connection = $this->databaseConnection;
      $connection->delete('migrate_file_singularity_hash')
        ->condition('fid', $id['fid'])
        ->execute();
      $connection->delete('migrate_file_singularity_map')
        ->condition('destid', $id['fid'])
        ->execute();
    }
  }

  /**
   * Helper method to check if we are migrating files.
   *
   * @param \Drupal\migrate\Plugin\Migration $migration
   *   The migration object.
   *
   * @return bool
   *   True if we are migrating files, false otherwise.
   */
  protected function isFilesMigration(Migration $migration) {
    $source_configuration = $migration->getSourceConfiguration();
    $destination_configuration = $migration->getDestinationConfiguration();
    return !empty($source_configuration['file_singularity']) && $destination_configuration['plugin'] === 'entity:file';
  }

}
